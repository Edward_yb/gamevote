#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <string.h>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void SetCookie();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    QString http_post_str;
    int voteflag;  // 0 - legend; 1 - legend mobile
    QString cookies;
    QTcpSocket *m_socket;
    QString m_ip;
    int m_port;

};

#endif // MAINWINDOW_H
